'use strict';

const requireDir = require('require-dir');

// Require all tasks in gulp/talks, including subfolders
requireDir('./gulp/tasks', { recurse: true });