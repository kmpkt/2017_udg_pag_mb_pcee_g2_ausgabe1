'use strict';

module.exports = {
  src: './src',
  build: './build',
  dist: './dist',
};
