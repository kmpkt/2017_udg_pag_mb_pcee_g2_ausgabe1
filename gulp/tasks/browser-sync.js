'use strict';

const gulp = require('gulp');
const browserSync = require('browser-sync');
const config = require('../config');
const src = 'src',
  dest = 'build';

// gulp.task('browsersync', function() {
gulp.task('browsersync', ['build'], function() {
  browserSync(
    {
      server: {
        baseDir: [dest, src]
        // , directory: true
      },
      ghostMode: true,
      files: [
        src + '/**/*',
        dest + '/**/*'
      ]
      // files: [
      //   src + '/**/*.html',
      //   dest + '/**',
      //   '!' + dest + '/**/*.map',
      //   '!' + dest + '/**/*.{png,jpg,gif}'
      // ]
    }
  );
});
