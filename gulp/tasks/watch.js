'use strict';

const gulp = require('gulp');
const browserSync = require('browser-sync');

const src = './src',
  dest = './build';

// gulp.task('watch', ['browsersync'], function() {
//   gulp.watch(src, {base: src});
// });

gulp.task('watch', function() {
  browserSync({server: {baseDir: src}}, function() {
    gulp.watch(src + '/**').on('change', function(file) {
      browserSync.reload(file);
    });
  });
});