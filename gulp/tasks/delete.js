const gulp = require('gulp');
const del = require('del');
const config = require('../config');

/**
 * Delete folders and files
 */
gulp.task('delete', function() {
  console.log(config.build);
  return del(
    config.build + '/**'
  );
});