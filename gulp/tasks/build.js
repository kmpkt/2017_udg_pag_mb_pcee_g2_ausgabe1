'use strict';

const gulp = require('gulp');
const runSequence = require('run-sequence');

gulp.task('build', function(cb) {
  runSequence('delete', 'copy-files',
    cb);
});