'use strict';

const gulp = require('gulp');
const watch = require('gulp-watch');
const browserSync = require('browser-sync');
const config = require('../config');

gulp.task('copy-files', function() {
  gulp.src(config.src + '/**/*', {base: config.src})
  // .pipe(watch(src, {base: src}))
    .pipe(gulp.dest(config.build))
    // .pipe(browserSync.reload({stream: true}));
});