var clickTag = "http://www.porsche.com/germany/"
var porsche = porsche || {};
porsche.main = function() {

  // banner settings
  var width = 300,
    height = 250,
    video,
    video1,
    video3,
    video4,
    imageLoaded = false,
    videoLoaded = false,
    bannerVisible = false;

  // create main container
  var banner = porsche.createElement({id: "banner", width: width, height: height, backgroundColor: "#ffffff", overflow: "hidden", cursor: "pointer", parent: document.body, boxSizing: "border-box", border: "solid 1px #c4c4c4"});
  var tl = new TimelineLite({paused: true});

  banner.finished = false;

  var images = [
    "assets/images/bg.jpg",
    "assets/images/poster.jpg",
    "assets/images/logo.jpg",
    "assets/images/icon_pause.svg",
    "assets/images/icon_mute.svg",
    "assets/images/icon_unmute.svg",
    "assets/images/icon_replay.svg",
    "assets/images/icon_play.svg",
    "assets/images/icon_close.png",
    "assets/images/txt_disclaimer.png",
    "assets/images/txt_1.png",
    "assets/images/txt_2.png",
    "assets/images/txt_3.png",
    "assets/images/cta_box.png",
    "assets/images/cta_boxHover.png",
    "assets/images/pag_loader_inner.png",
    "assets/images/pag_loader_outer.png",
    "assets/images/icon_video.png"
  ];

  function onPolite() {

    porsche.preloadImages(images, function() {

      ///////////////////////
      // C R E A T E  D I V S

      var bg = porsche.createElement({id: "background", backgroundImage: "assets/images/bg.jpg", top: 71, retina: false, parent: banner});
      var logo = porsche.createElement({backgroundImage: "assets/images/logo.jpg", left: 108, top: 15, retina: true, parent: banner});

      var txt = porsche.createElement({left: 13, top: 80, width: 256, height: 150, parent: banner});
      var txt_3 = porsche.createElement({backgroundImage: "assets/images/txt_3.png", left: 3, top: 6, retina: true, parent: txt});
      var txt_2 = porsche.createElement({backgroundImage: "assets/images/txt_2.png", left: 2, top: 5, retina: true, parent: txt});
      var txt_1 = porsche.createElement({backgroundImage: "assets/images/txt_1.png", left: 3, top: 4, retina: true, parent: txt});
      var txt_disclaimer = porsche.createElement({backgroundImage: "assets/images/txt_disclaimer.png", left: -2, top: 133, centerX: true, retina: true, parent: txt});

      //////// <autoplay-fix
      var poster = porsche.createElement({backgroundImage: "assets/images/poster.jpg", top: 71, retina: false, parent: banner});
      poster.hide();
      //////// autoplay-fix>

      var icons = porsche.createElement({id: "icons", width: 300, height: 250, zIndex: 2, parent: banner});
      var loader = porsche.createElement({id: "loader", width: 300, height: 250, parent: icons});
      var icon_pause = porsche.createElement({backgroundImage: "assets/images/icon_pause.svg", left: 220, bottom: 143, parent: icons});
      var icon_mute = porsche.createElement({backgroundImage: "assets/images/icon_mute.svg", left: 260, bottom: 143, parent: icons});
      var icon_unmute = porsche.createElement({backgroundImage: "assets/images/icon_unmute.svg", left: 260, bottom: 143, parent: icons});
      var icon_replay = porsche.createElement({backgroundImage: "assets/images/icon_replay.svg", left: 265, bottom: 148, parent: icons});
      var icon_play = porsche.createElement({backgroundImage: "assets/images/icon_play.svg", left: 110, top: 120, parent: icons});
      var icon_close = porsche.createElement({backgroundImage: "assets/images/icon_close.png", left: 275, top: -100, display: "none", retina: true, parent: icons});

      var containerParent = porsche.createElement({left: 0, top: 71, width: 1, height: 1, parent: banner});
      var video_container1 = porsche.createElement({id: "video_container1", top: 71, width: 300, height: 179, top: 0, opacity: 0, parent: containerParent});
      var video_container3 = porsche.createElement({id: "video_container3", top: 71, width: 300, height: 179, top: 0, opacity: 0, parent: containerParent});
      var video_container4 = porsche.createElement({id: "video_container4", top: 71, width: 300, height: 179, top: 0, opacity: 0, parent: containerParent});
      var video_container = porsche.createElement({id: "video_container", top: 71, width: 300, height: 179, top: 0, parent: containerParent});
      var buttons = porsche.createElement({left: 0, top: 0, width: 1, height: 1, zIndex: 25, opacity: 0, parent: banner});
      var icon_video1 = porsche.createElement({backgroundImage: "assets/images/icon_video.png", left: 55, top: 155, opacity: 0.7, retina: true, parent: buttons});
      var icon_video3 = porsche.createElement({backgroundImage: "assets/images/icon_video.png", left: 152, top: 135, opacity: 0.7, retina: true, parent: buttons});
      var icon_video4 = porsche.createElement({backgroundImage: "assets/images/icon_video.png", left: 210, top: 156, opacity: 0.7, retina: true, parent: buttons});

      //////// <autoplay-fix
      if (device.ios()) {
        TweenLite.set([icon_mute, icon_unmute], {visibility: "hidden"});
        TweenLite.set(icon_pause, {left: icon_mute.get("left"), top: icon_mute.get("top")});
      }
      //////// autoplay-fix>

      TweenLite.set(txt_disclaimer, {opacity: 0})
      TweenLite.set([bg, video_container], {borderRight: "solid 1px #c8c8c8"});
      buttons.set({pointerEvents: "none"});

      ///////////////////////
      // V I D E O

      video1 = porsche.createElement({
        type: "video",
        id: "video",
        sources: [
          {url: Enabler.getUrl("assets/videos/porsche1.mp4"), type: "video/mp4"},
        ],
        //autoplay: true,
        loader: loader,
        container: video_container,
        preload: "auto",
        muted: true,
        controls: false,
        parent: video_container1,
      });

      video3 = porsche.createElement({
        type: "video",
        id: "video",
        sources: [
          {url: Enabler.getUrl("assets/videos/porsche3.mp4"), type: "video/mp4"},
        ],
        // autoplay: true,
        loader: loader,
        container: video_container,
        width: video_container.get("width"),
        preload: "auto",
        muted: true,
        controls: false,
        parent: video_container3,
      });

      video4 = porsche.createElement({
        type: "video",
        id: "video",
        sources: [
          {url: Enabler.getUrl("assets/videos/porsche4.mp4"), type: "video/mp4"},
        ],
        // autoplay: true,
        loader: loader,
        container: video_container,
        width: video_container.get("width"),
        preload: "auto",
        muted: true,
        controls: false,
        parent: video_container4,
      });

      video1.addEventListener("ended", onVideoEnded);
      video3.addEventListener("ended", onVideoEnded);
      video4.addEventListener("ended", onVideoEnded);

      video1.addEventListener("play", onVideoPlay);
      video3.addEventListener("play", onVideoPlay);
      video4.addEventListener("play", onVideoPlay);

      function onVideoPlay(event) {
        buttons.to(0.1, {opacity: 0});
        txt.to(0.1, {opacity: 0});
        icon_replay.to(0.1, {opacity: 0});
        icon_mute.to(0.1, {opacity: 0});
        icon_unmute.to(0.1, {opacity: 0});
        icon_close.to(0.1, {opacity: 0});
      }

      function onVideoEnded(event) {
        video_container1.to(0.2, {opacity: 0});
        video_container3.to(0.2, {opacity: 0});
        video_container4.to(0.2, {opacity: 0});
        txt.to(0.1, {opacity: 1});
        icon_replay.to(0.1, {opacity: 1});
        //icon_mute.to(0.1, {opacity:1});
        //icon_unmute.to(0.1, {opacity:1});
        icon_close.to(0.1, {opacity: 1});
        buttons.to(0.1, {opacity: 1});
      }

      //video.addEventListener("ended", showTxt)
      //video.addEventListener("play", hideTxt)

      video = porsche.createVideo({
        filename: "porsche",
        // filename: "porsche_short",
        container: video_container,
        loader: loader,
        controls: {
          play: icon_play,
          pause: icon_pause,
          mute: icon_mute,
          unmute: icon_unmute,
          replay: icon_replay,
          close: icon_close
        },
        onLoaded: function() {
          videoLoaded = true;
          TweenLite.to(txt_disclaimer, .5, {opacity: 1, delay: 1});
          if (bannerVisible) {
            video.autoPlay(); //////// autoplay-fix
          }
        },
        preEndedDelay: .3,
        onPreEnded: function() {
          tl.resume();
        },
        onEnded: function() {

        },
        onReplay: function() {
          tl.pause("start");
          txt_3.set({opacity: 0});
          buttons.set({pointerEvents: "none"});
        }
      })

      //////// <autoplay-fix
      video.autoPlay = function() {

        var autoPlayed = false;

        video.addEventListener("play", onPlay);
        function onPlay() {
          autoPlayed = true;
          video.removeEventListener("play", onPlay);
        };
        video.play();

        setTimeout(function() {
          if (!autoPlayed) {
            icon_play.show();
            poster.show();
            video_container.hide();
            icon_pause.hide();
            video.addEventListener("play", function() {
              poster.hide();
              video_container.show();
            });
          } else {
            //console.log("autoplayed!")
          }
        }, 100);

      }
      ///////// autoplay-fix>

      ///////////////////////
      // A N I M A T I O N

      // Custom animation
      HTMLDivElement.prototype.animateIn = function() {
        var speed = .05;
        var ease = Power0.easeNone;
        this.set({opacity: 0.0})
        this.in = new TimelineLite()
          .to(this, speed, {opacity: 0.5, ease: ease})
          .to(this, speed, {opacity: 0.0, ease: ease})
          .to(this, speed, {opacity: 1.0, ease: ease})
          .to(this, speed / 2, {opacity: 0.8, ease: ease})
          .to(this, speed / 2, {opacity: 1.0, ease: ease})
      }

      copyTl = new TimelineLite({paused: true})
        .from(txt_1, 0.3, {x: -4, opacity: 0})
        .to(txt_1, 0.3, {x: 4, opacity: 0}, "+=2.5")
        .from(txt_2, 0.3, {x: -4, opacity: 0})
        .to(txt_2, 0.3, {x: 4, opacity: 0}, "+=1.5")
        .from(txt_3, 0.3, {x: -4, opacity: 0})
        .set(buttons, {pointerEvents: "auto"})

      // Main Timeline
      tl
        .addLabel("start")
        .set(video_container, {opacity: 1})
        .addLabel("videoEnd")
        .to(video, .3, {opacity: 0})
        .from(bg, .3, {opacity: 0}, "videoEnd")
        .from(buttons, 0.3, {opacity: 0})
        .add(function() {
          copyTl.play(0);
        })
        .to(buttons, 0.3, {opacity: 1}, "=+6")

      ///////////////////////
      // I N T E R A C T I O N

      // Fade out & In buttons when playing video
      icon_video1.addEventListener("mouseenter", buttonIn);
      icon_video3.addEventListener("mouseenter", buttonIn);
      icon_video4.addEventListener("mouseenter", buttonIn);
      function buttonIn(event) {
        TweenLite.to(event.target, 0.3, {opacity: 1});

      }

      icon_video1.addEventListener("mouseleave", buttonOut);
      icon_video3.addEventListener("mouseleave", buttonOut);
      icon_video4.addEventListener("mouseleave", buttonOut);
      function buttonOut(event) {
        TweenLite.to(event.target, 0.3, {opacity: 0.7});
      }

      //Button click, sorry for not DRY
      icon_play.addEventListener('click', function(e) {
        e.stopPropagation();
        video.play();
        icon_play.to(0.1, {opacity: 0});
      })

      icon_video1.addEventListener("click", function(e) {
        video_container1.to(0.2, {opacity: 1})
        e.stopPropagation();
        video1.play();
        icon_play.to(0.1, {opacity: 0});
        onVideoPlay();
      })

      icon_video3.addEventListener("click", function(e) {
        video_container3.to(0.2, {opacity: 1})
        e.stopPropagation();
        video3.play();
        icon_play.to(0.1, {opacity: 0});
        onVideoPlay();
      })

      icon_video4.addEventListener("click", function(e) {
        video_container4.to(0.2, {opacity: 1})
        e.stopPropagation();
        video4.play();
        icon_play.to(0.1, {opacity: 0});
        onVideoPlay();
      })

      icon_close.addEventListener("click", function(e) {
        tl.seek('-=0', false);
        copyTl.play(0);
      })

    });
  }

  function onVisible() {
    bannerVisible = true;
    if (videoLoaded)
      video.autoPlay();
  }

  /*	// Remove before publishing
   if(getUrlVars().debug) return banner.onclick = function(){video.pause();window.open(clickTag)}, onPolite(), onVisible();
   function getUrlVars() {
   var vars = {};
   var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
   vars[key] = value;
   });
   return vars;
   }*/

  // set up DoubleClick listeners
  porsche.dc.init({
    onInit: function() {
      // add click listener
      banner.addEventListener("click", function(e) {
        if (!banner.finished) {
          video.pause();
          console.log("banner.finished: " + banner.finished);
        }
        Enabler.exit("Exit");
        //video.pause();
      });
    },
    onPolite: onPolite,
    onVisible: onVisible
  });
}

porsche.createVideo = function(parameters) {

  ///////////////////////
  // L O A D E R

  var loader = porsche.createElement({
    width: parameters.loader.get("width"),
    height: parameters.loader.get("height"),
    zIndex: 10,
    pointerEvents: "none",
    backgroundColor: "#fff",
    parent: parameters.loader
  })
  loader.in = porsche.createElement({backgroundImage: "assets/images/pag_loader_inner.png", parent: loader}).center();
  loader.out = porsche.createElement({backgroundImage: "assets/images/pag_loader_outer.png", parent: loader}).center();
  loader.tl = new TimelineLite({
    onComplete: function() {
      loader.tl.play(0)
    }
  })
    .to(loader.in, 1, {rotation: "-=360", ease: Power0.easeNone}, 0)
    .to(loader.out, 1, {rotation: "+=360", ease: Power0.easeNone}, 0);

  ///////////////////////
  // V I D E O

  var video = porsche.createElement({
    type: "video",
    id: "video",
    sources: [
      {url: Enabler.getUrl('assets/videos/' + parameters.filename + '.mp4'), type: "video/mp4"},
      // {url: Enabler.getUrl(parameters.filename+'.webm'), type: "video/webm"},
      // {url: Enabler.getUrl(parameters.filename+'.ogg'), type: "video/ogg"},
    ],
    autoplay: false,
    height: video_container.get("height"),
    preload: "auto",
    muted: true,
    controls: false,
    parent: parameters.container
  });
  parameters.container.video = video;
  parameters.controls.play.hide();
  parameters.controls.pause.hide();
  parameters.controls.mute.hide();
  parameters.controls.unmute.hide();
  parameters.controls.replay.hide();
  video.preEnded = false;
  video.load();
  video.pause();
  // SDK Tracking Added ----
  Enabler.loadModule(studio.module.ModuleId.VIDEO, function() {
    studio.video.Reporter.attach('porsche_video', video);
  });
  // -

  ///////////////////////
  // E V E N T S

  video.addEventListener('loadeddata', function() {
    parameters.controls.pause.show();
    parameters.controls.play.hide();
    if (video.muted)
      parameters.controls.unmute.show();
    else
      parameters.controls.mute.show();
    loader.tl.pause();
    loader.set({opacity: 0})
    parameters.onLoaded();
  }, false);

  video.addEventListener('ended', function() {
    parameters.controls.replay.show();
    parameters.controls.play.hide();
    parameters.controls.pause.hide();
    parameters.controls.mute.hide();
    parameters.controls.unmute.hide();
    parameters.onEnded();
    banner.finished = true;
  }, false);

  video.addEventListener('timeupdate', function() {
    if (video.currentTime > (video.duration - parameters.preEndedDelay) && !video.preEnded) {
      video.preEnded = true;
      parameters.onPreEnded();
    }
  }, false)
  video.addEventListener('pause', function() {
    parameters.controls.play.show();
    parameters.controls.pause.hide();
  }, false)
  parameters.controls.play.addEventListener('click', function(e) {
    e.stopPropagation();
    parameters.controls.play.hide();
    parameters.controls.mute.show();
    parameters.controls.pause.show();
    video.play();
    //firstFrame.to(0.2, {opacity:0})
  }, true)
  parameters.controls.pause.addEventListener('click', function(e) {
    e.stopPropagation();
    video.pause();
  })
  parameters.controls.close.addEventListener('click', function(e) {
    e.stopPropagation();
    parameters.controls.play.hide();
    parameters.controls.mute.hide();
    parameters.controls.pause.hide();
    parameters.controls.replay.show();
    video.muted = true;
    //firstFrame.to(0.2, {opacity:0})
  }, true)
  parameters.controls.mute.addEventListener('click', function(e) {
    e.stopPropagation();
    video.muted = true;
    parameters.controls.unmute.show();
    parameters.controls.mute.hide();
  })
  parameters.controls.unmute.addEventListener('click', function(e) {
    e.stopPropagation();
    video.muted = false;
    parameters.controls.mute.show();
    parameters.controls.unmute.hide();
  })
  parameters.controls.replay.addEventListener('click', function(e) {
    e.stopPropagation();
    video.preEnded = false;
    video.currentTime = 0;
    video.play();
    parameters.onReplay();
    parameters.controls.replay.hide();
    parameters.controls.play.hide();
    parameters.controls.pause.show();
    parameters.controls.mute.hide();
    banner.finished = false;
    if (video.muted)
      parameters.controls.unmute.show();
    else
      parameters.controls.mute.show();
  })

  return video;
}

HTMLDivElement.prototype.show = function() {
  this.set({opacity: 1, pointerEvents: "auto", y: 0})
}
HTMLDivElement.prototype.hide = function() {
  this.set({opacity: 0, pointerEvents: "none", y: 9000})
}
