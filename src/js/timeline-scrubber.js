porsche.scrubber = function(timelines) {

  console.log("=== Banner timeline enabled ===");

  porsche.scrubberController = function(tl) {

    var labels = [];
    var labelNames = [];
    var i = 0;

    //create main container
    var mainContainer = this.mainContainer = porsche.createElement({id: "toggleContainer", width: 530, height: 80, position: "fixed", bottom: 0, left: 30});

    //create toggle container
    var toggleContainer = this.toggleContainer = porsche.createElement({id: "toggleContainer", width: 560, height: 50, top: 20, paddingTop: "5px", paddingLeft: "5px", overflow: "hidden"});

    //var handle = this.handle = porsche.createElement({innerHTML: "||", left: -10, width:8, height: 20, color:"#ef5c63", lineHeight: "21px", position: "absolute"});

    //create name
    var name = this.name = porsche.createElement({innerHTML: "timeline", color: "#ef5c63", fontFamily: "verdana", userSelect: "none", cursor: "pointer"});

    //create container
    var container = porsche.createElement({width: 523, height: 30, float: "left", position: "relative"});

    //create background
    var bg = porsche.createElement({width: 520, height: 10, background: "#36b1c5", borderRadius: 10, position: "absolute", boxShadow: "0 0 0 3px #86c8d8", marginLeft: -10, parent: container});

    //create time indicator
    var time = porsche.createElement({innerHTML: "0", left: 535, top: 4, color: "#ef5c63", fontFamily: "verdana", fontSize: 10, userSelect: "none"})

    // create time forward button
    var timeStepForward = porsche.createElement({innerHTML: "^", left: 555, top: -1, color: "#ef5c63", fontFamily: "verdana", fontWeight: "bold", fontSize: 12, cursor: "pointer", userSelect: "none"});

    // create time backward button
    var timeStepBackward = porsche.createElement({innerHTML: "^", left: 555, top: 10, color: "#ef5c63", fontFamily: "verdana", fontWeight: "bold", rotation: 180, fontSize: 12, cursor: "pointer", userSelect: "none"});

    //create labels
    for (var label in tl._labels) {
      labels[i] = tl._labels[label];
      labelNames[i] = label;
      porsche.createElement({background: "#0e2c31", width: 2, height: 20, left: 510 * (tl._labels[label] / tl.totalDuration()) + 4, parent: container});
      porsche.createElement({innerHTML: labelNames[i], fontFamily: "verdana", fontSize: 9, left: 510 * (tl._labels[label] / tl.totalDuration()) + 9, top: 12, userSelect: "none", parent: container});
      i++;
    }

    // create scrubbing ball
    var ball = porsche.createElement({id: "scrubberBall", width: 10, height: 10, borderRadius: 10, background: "#0e2c31", position: "absolute", left: 0, marginLeft: -5, cursor: "pointer", parent: container});

    //create play / pause playButton container
    var buttonContainer = porsche.createElement({id: "playButton", width: 20, height: 20, position: "absolute", left: 236, top: 31});

    //create play / pause button
    var playButton = porsche.createElement({width: 10, height: 0, borderRadius: 2, borderTop: "10px solid transparent", borderBottom: "10px solid transparent", borderLeft: "10px solid #ef5c63", background: "#ef5c63", cursor: "pointer", position: "absolute"});

    //create rewind button
    var rewindButton = porsche.createElement({innerHTML: "|<", position: "absolute", left: 200, top: 40, fontFamily: "verdana", fontSize: "14px", letterSpacing: "-3px", margin: 0, lineHeight: 0, userSelect: "none", cursor: "pointer", color: "#ef5c63"});

    //create forward button
    var forwardButton = porsche.createElement({innerHTML: ">|", position: "absolute", left: 279, top: 40, fontSize: "14px", fontFamily: "verdana", letterSpacing: "-3px", margin: 0, lineHeight: 0, userSelect: "none", cursor: "pointer", color: "#ef5c63"});

    //add to DOM
    toggleContainer.appendChild(container);
    buttonContainer.appendChild(playButton);
    toggleContainer.appendChild(buttonContainer);
    toggleContainer.appendChild(rewindButton);
    toggleContainer.appendChild(forwardButton);
    mainContainer.appendChild(name);
    toggleContainer.appendChild(time);
    toggleContainer.appendChild(timeStepForward);
    toggleContainer.appendChild(timeStepBackward);
    mainContainer.appendChild(toggleContainer);
    document.body.appendChild(mainContainer);

    var scrubbing = false;
    var dragging = false;
    var clickedName = false;
    var mousePos = 0;
    var scrubPos = 0;
    var timelinePos = 0;
    var offsetX = 0;
    var offsetY = 0;

    //click scrub bar
    container.addEventListener("mousedown", function() {
      scrubbing = true;
      setScrubPos();
    });

    //leave scrub bar
    container.addEventListener("mouseleave", function() {
      scrubbing = false;
    });

    // release scrub bar
    container.addEventListener("mouseup", function() {
      scrubbing = false;
    });

    // get mouse pos
    container.addEventListener("mousemove", function(event) {
      mousePos = event.clientX - (mainContainer.offsetLeft - mainContainer.scrollLeft);
      setScrubPos();
    });

    // set the scrubber position
    function setScrubPos() {
      //calculate new scrubber position (clamp between 10 and 510)
      scrubPos = Math.min(Math.max(mousePos, Math.min(10, 520)), Math.max(10, 520)) - 10;

      if (scrubbing) {
        ball.set({left: scrubPos});
        //set position of timeline
        tl.progress(scrubPos / 510);
      } else {
        ball.set({left: 510 * timelinePos});
      }
    }

    //play / pause
    playButton.addEventListener("click", function() {
      if (tl.isActive()) {
        tl.pause();
      } else {
        tl.play();
      }
    });

    //skip backwards
    var closestSmallerNumber = null;
    var difference = null;
    rewindButton.addEventListener("click", function() {

      for (var i = 0; i < labels.length; i++) {
        if (labels[i] < tl.time()) {

          difference = tl.time() - labels[i];

          if (tl.time() - labels[i] <= difference) {
            closestSmallerNumber = labels[i];
            difference = tl.time() - closestSmallerNumber;
          }
        }

        if (closestSmallerNumber == tl.time() || closestSmallerNumber == null) closestSmallerNumber = 0;
      }

      tl.pause();
      tl.seek(closestSmallerNumber);
    });

    //skip forward
    var closestBiggerNumber = null;
    forwardButton.addEventListener("click", function() {

      for (var i = labels.length; i >= 0; i--) {
        if (labels[i] > tl.time()) {

          difference = labels[i] - tl.time();

          if (labels[i] - tl.time() <= difference) {
            closestBiggerNumber = labels[i];
            difference = closestBiggerNumber - tl.time();
          }
        }
      }

      if (closestBiggerNumber == tl.time() || closestBiggerNumber == null) closestBiggerNumber = tl.totalDuration();

      tl.pause();
      tl.seek(closestBiggerNumber);
    });

    //seek forward 0.1 seconds
    timeStepForward.addEventListener("click", function() {
      tl.pause();
      tl.seek(tl.time() + 0.1);
    });

    //seek backward 0.1 seconds
    timeStepBackward.addEventListener("click", function() {
      tl.pause();
      tl.seek(tl.time() - 0.1);
    });

    function getClosest(number, array) {
      var closest = null;
      for (var item in array) {
        if (closest == null || Math.abs(number - closest) > Math.abs(item - number)) {
          closest = item;
        }
      }
      return closest;
    }

    //toggle controller
    name.addEventListener("click", function() {
      if (dragging) return;
      if (toggleContainer.get("height") == 60) {
        TweenLite.to(toggleContainer, 0.2, {height: 0, paddingTop: 0});
      } else {
        TweenLite.to(toggleContainer, 0.2, {height: 60, paddingTop: 5});
      }
    });

    //drag controller
    name.addEventListener("mousedown", function(event) {
      offsetX = event.clientX - mainContainer.offsetLeft;
      offsetY = event.clientY - mainContainer.offsetTop;
      clickedName = true;
    });

    //release controller
    name.addEventListener("mouseup", function() {
      clickedName = false;
      //bug prevention
      TweenLite.delayedCall(0.1, function() {
        dragging = false;
      })
    });

    //drag controller
    window.addEventListener("mousemove", function(event) {
      var mouseX = event.clientX;
      var mouseY = event.clientY;
      if (clickedName) dragging = true;

      if (dragging) {
        mainContainer.set({left: mouseX - offsetX, top: mouseY - offsetY});
      }
    });

    //update loop
    (function update() {
      // update timeline position variable
      timelinePos = tl.progress();

      // set time text
      time.set({innerHTML: Math.round(tl.time() * 10) / 10});

      //set scrubber position
      setScrubPos();

      //update play / pause button
      if (tl.isActive()) {
        playButton.set({background: "#ef5c63", x: 0});
      } else {
        playButton.set({background: "", x: 7});
      }

      requestAnimationFrame(update);
    })();
  }

  //global variables
  var scrubbers = [];
  var i = 0;

  //create scrubbers
  for (var _tl in timelines) {
    scrubbers[i] = new porsche.scrubberController(timelines[_tl]);
    scrubbers[i].name.innerHTML = "|| " + _tl + " ||";
    scrubbers[i].mainContainer.set({bottom: 80 * i});
    i++;
  }
}




